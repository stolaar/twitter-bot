import dayjs from 'dayjs'
import { AMPM } from '@components/common/DatePicker/TimePicker'

export const dateToISOFormat = (date: Date | string | null): string | null => {
  return date ? dayjs(date).toISOString() : null
}

export const getAMPMTime = (date: Date | null): string | null => {
  return date ? dayjs(date).format('hh:mm A') : null
}

export const get24HoursFormat = (
  date: Date | string | null,
  AMPM: AMPM,
): string | null => {
  return date ? dayjs(`${date} ${AMPM}`, ['hh A']).format('HH') : null
}

export const getAMorPM = (date: Date | string): 'AM' | 'PM' => {
  return dayjs(date).format('A') as 'AM' | 'PM'
}

export const get12HoursFormat = (hours: string, AMPM?: AMPM) => {
  return AMPM
    ? dayjs(`${hours} ${AMPM}`, 'hh A').format('hh')
    : dayjs(hours, 'HH').format('hh')
}

export const isBetweenDates = (
  date: string | null,
  minDate?: Date | string | null,
  maxDate?: Date | string | null,
): boolean => {
  const dateToCheck = dayjs(date)
  return (
    (minDate ? dateToCheck.isAfter(minDate) : true) &&
    (maxDate ? dateToCheck.isBefore(maxDate) : true)
  )
}
