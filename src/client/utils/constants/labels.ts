export const BUTTON_LABELS = Object.freeze({
  submit: 'Submit',
  reSchedule: 'Re-schedule',
  stop: 'Stop',
})

export const FIELD_LABELS = Object.freeze({
  hashtags: 'Hashtags',
  query: 'Query',
  cron: 'Cron expression',
  postText: 'Post text',
  postMedia: 'Post media',
})

export const FIELD_PLACEHOLDERS = Object.freeze({
  hashtags: '#hashtag1, #hashtag2',
  cron: '1 * * * * *',
})

export const FIELD_HELPER_TEXTS = Object.freeze({
  hashtags: 'Comma separated list of hashtags',
  cron: 'How often to run the job',
})
