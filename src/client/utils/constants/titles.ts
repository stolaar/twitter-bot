export const TITLES = Object.freeze({
  followByHashTag: 'Follow by hashtag',
  followByHashTagSubtitle: 'Schedules task for following users by hashtag',
  retweetPosts: 'Retweet posts',
  schedulePosts: 'Schedule Posts',
  runningTaskInfo: 'Task running',
})
