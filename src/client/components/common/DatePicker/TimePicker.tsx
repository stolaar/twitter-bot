import * as React from 'react'
import { useEffect, useState } from 'react'
import {
  ClickAwayListener,
  InputAdornment,
  Paper,
  Popover,
  TextField,
} from '@mui/material'
import { Switch } from './Switch'
import IconButton from '@mui/material/IconButton'
import { AccessTime as TimePickerIcon } from '@mui/icons-material'
import {
  getAMorPM,
  getAMPMTime,
  get12HoursFormat,
  get24HoursFormat,
} from '@client-services/date'
import { styled } from '@mui/material/styles'
function addPrefix(value: number): string {
  return value.toString().padStart(2, '0')
}

export type AMPM = 'PM' | 'AM'

function transformHours(
  nextHours: number,
  isAMPM: boolean,
  AMPM: AMPM | null,
): string | null {
  if (Number.isNaN(nextHours)) return null

  if (isAMPM) {
    if (!AMPM) return null
    return get24HoursFormat(nextHours.toString(), AMPM)
  }
  if (nextHours > 23 && nextHours < 0) return null

  return addPrefix(nextHours)
}

function transformMinutes(minutes: number): string | void {
  if (Number.isNaN(minutes)) return
  if (minutes > 59) {
    return
  }
  return addPrefix(minutes)
}

function displayTime(
  hours: string,
  minutes: string,
  date: Date | null,
  isAMPM: boolean,
) {
  if (!isAMPM) {
    return `${hours}:${minutes}`
  }
  return getAMPMTime(date)
}

function displayHours(hours: string, isAMPM: boolean, AMPM: AMPM | null) {
  if (!isAMPM) {
    return hours || ''
  }
  if (!AMPM) return
  return get12HoursFormat(hours, AMPM)
}

export default function TimePicker({
  isAMPM = true,
  time,
  onChange,
  onAMPMChange,
  error,
  disabled,
}: {
  isAMPM: boolean
  time?: Date | null
  onChange: (h: number, m: number, AMPM: AMPM | null) => void
  onAMPMChange: any
  error: boolean
  disabled: boolean
}) {
  const [anchorEl, setAnchorEl] = useState<HTMLInputElement | null>(null)
  const [AMPM, setAMPM] = useState<AMPM | null>(null)

  const handleClick = (event: any) => {
    if (disabled) return
    setAnchorEl(event.currentTarget)
  }

  useEffect(() => {
    if (AMPM) {
      onAMPMChange(AMPM)
    }
  }, [AMPM, onAMPMChange])

  useEffect(() => {
    if (time && !AMPM) {
      setAMPM(getAMorPM(time))
    }
  }, [time, AMPM])

  return (
    <>
      <Paper
        onClick={handleClick}
        component={'form'}
        sx={{
          pl: 2,
          display: 'flex',
          alignItems: 'center',
          border: 0,
          boxShadow: 'none',
        }}
      >
        <TextField
          label={'Time'}
          name={'time'}
          sx={{ width: '130px' }}
          error={error}
          disabled={disabled}
          value={
            displayTime(
              time?.getHours().toString() || '',
              time?.getMinutes().toString() || '',
              time || null,
              isAMPM,
            ) || ''
          }
          InputProps={{
            endAdornment: (
              <InputAdornment position={'end'}>
                <IconButton
                  className={'.MuiInputBase-root'}
                  color="default"
                  sx={{
                    borderRadius: 0,
                    p: 0,
                  }}
                  aria-label="directions"
                >
                  <TimePickerIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Paper>
      <Popover
        open={!!anchorEl}
        anchorEl={anchorEl}
        anchorPosition={{ top: 0, left: 0 }}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        sx={{ width: '190px' }}
      >
        <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '100%',
            }}
          >
            {isAMPM ? (
              <Switch
                onChange={() =>
                  setAMPM((prev) => (prev === 'AM' ? 'PM' : 'AM'))
                }
                checked={AMPM === 'PM'}
                sx={{ mt: '10px' }}
              />
            ) : null}
            <div style={{ margin: 5, width: '150px', display: 'flex' }}>
              <TimeInput
                variant={'filled'}
                label={'Hour'}
                sx={{ borderRight: '1px solid #d3d3d3' }}
                value={displayHours(
                  time?.getHours().toString() || '',
                  isAMPM,
                  AMPM,
                )}
                onChange={(e) => {
                  const hours = transformHours(
                    parseInt(e.target.value),
                    isAMPM,
                    AMPM,
                  )
                  if (!hours || (!AMPM && !isAMPM)) return
                  onChange &&
                    onChange(parseInt(hours), time?.getMinutes() || 0, AMPM)
                }}
              />
              <TimeInput
                type={'text'}
                label={'Min.'}
                variant={'filled'}
                value={time?.getMinutes().toString() || ''}
                onChange={(e) => {
                  const min = transformMinutes(parseInt(e.target.value))
                  if (!min || (!AMPM && !isAMPM)) return
                  onChange &&
                    onChange(time?.getHours() || 0, parseInt(min), AMPM)
                }}
              />
            </div>
          </div>
        </ClickAwayListener>
      </Popover>
    </>
  )
}
//
// export const TimeInput: React.FC<TextFieldProps> = (props) => {
//   return <TextField variant={'filled'} {...props} />
// }

const TimeInput = styled(TextField)({
  '& label.Mui-focused': {
    color: '#525252',
    '& .MuiInput-underline:after': {
      borderBottomColor: '#525252',
    },
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#525252',
  },
  '& .MuiInputBase-input': {
    backgroundColor: '#F5F5F5',
    borderRadius: '4px',
  },
})
