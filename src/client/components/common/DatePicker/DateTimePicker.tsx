import React from 'react'
import LocalizationProvider from '@mui/lab/LocalizationProvider'
import AdapterDayjs from '@mui/lab/AdapterDayjs'
import { useController, UseFormRegister } from 'react-hook-form'
import { TextField, TextFieldProps } from '@mui/material'
import { DateTimePicker as MUIDateTimePicker } from '@mui/x-date-pickers/DateTimePicker'
import { DatePickerProps as MUIDatePickerProps } from '@mui/x-date-pickers/DatePicker/DatePicker'

export interface DatePickerProps extends Partial<MUIDatePickerProps> {
  value?: Date | null
  name: string
  register?: UseFormRegister<any>
  control: any
}

export const DateTimePicker: React.FC<DatePickerProps & TextFieldProps> = ({
  label,
  name,
  control,
  ...rest
}): JSX.Element => {
  const {
    field: { onChange, value },
    formState: { errors },
  } = useController({ control, name })
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <MUIDateTimePicker
        renderInput={(props: TextFieldProps) => (
          <TextField
            {...props}
            sx={rest.sx}
            error={errors[name as string]}
            helperText={errors[name as string]?.message}
          />
        )}
        label={label}
        value={value}
        onChange={onChange}
        {...rest}
      />
    </LocalizationProvider>
  )
}
