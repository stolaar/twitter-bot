import { FormField } from '../../../formFields/common/FormField'
import { FC } from 'react'

export const FieldComponent: FC<{ formField: FormField }> = ({
  formField,
}): JSX.Element => {
  return formField.render()
}
