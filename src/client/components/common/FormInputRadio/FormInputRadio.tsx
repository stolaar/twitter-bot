import React, { useCallback } from 'react'
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  RadioProps,
} from '@mui/material'
import { UseFormRegister, Controller } from 'react-hook-form'

interface FormInputProps {
  name: string
  label: string
  setValue?: any
}

interface IRadioOption {
  label: string
  value: string | number | boolean
}

interface IRadioProps {
  register?: UseFormRegister<any>
  options: IRadioOption[]
  control: any
}

export type IRadioGroupProps = FormInputProps & IRadioProps & RadioProps

export const FormInputRadio: React.FC<IRadioGroupProps> = ({
  name,
  label,
  register,
  options,
  disabled,
  sx,
  control,
  defaultValue,
}) => {
  const generateRadioOptions = useCallback(() => {
    return options.map((singleOption) => (
      <FormControlLabel
        {...singleOption}
        key={singleOption.value.toString()}
        {...(register && register(name))}
        control={<Radio />}
      />
    ))
  }, [options, register])

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { name, value, onChange } }) => (
        <FormControl component="fieldset" disabled={disabled} sx={sx}>
          <FormLabel component="legend">{label}</FormLabel>
          <RadioGroup
            row
            onChange={onChange}
            defaultValue={defaultValue}
            value={value || ''}
            {...(register && register(name))}
          >
            {generateRadioOptions()}
          </RadioGroup>
        </FormControl>
      )}
    />
  )
}
