import { TextFieldProps, TextField as MUITextField } from '@mui/material'
import { Control, Controller } from 'react-hook-form'

export type ITextFieldProps = TextFieldProps & { control: Control }
export const TextField: React.FC<ITextFieldProps> = ({
  control,
  name,
  ...props
}) => {
  return (
    <Controller
      name={name || ''}
      control={control || null}
      render={({
        field: { value, onChange, onBlur },
        formState: { errors },
      }) => {
        return (
          <MUITextField
            {...props}
            value={value || ''}
            onChange={onChange}
            onBlur={onBlur}
            error={!!errors[name as string]}
            helperText={errors[name as string]?.message || props.helperText}
          />
        )
      }}
    />
  )
}
