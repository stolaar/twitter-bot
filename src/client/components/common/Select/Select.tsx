import { FormControl, MenuItem, TextField, TextFieldProps } from '@mui/material'
import { Controller, UseFormRegister } from 'react-hook-form'
import { FC } from 'react'

interface Option {
  value: string
  label: string
}

export interface Props {
  register: UseFormRegister<any>
  name: string
  options: Option[]
  control: any
}

export type SelectProps = TextFieldProps & Props

export const Select: FC<SelectProps> = ({
  name,
  options,
  sx,
  control,
  ...rest
}): JSX.Element => {
  return (
    <Controller
      defaultValue={''}
      name={name}
      control={control}
      render={({ field: { name, value, onChange, onBlur } }) => (
        <FormControl sx={sx}>
          <TextField
            {...rest}
            select
            onChange={onChange}
            onBlur={onBlur}
            value={value || ''}
            label="Choose one Person of trinity"
            id="trinity"
            name={name}
            defaultValue={''}
          >
            {options.map((person) => (
              <MenuItem key={person.value} value={person.value}>
                {person.label}
              </MenuItem>
            ))}
          </TextField>
        </FormControl>
      )}
    />
  )
}
