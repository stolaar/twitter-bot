import { styled } from '@mui/material/styles'
import { Box, Button, Paper, Typography } from '@mui/material'
import { mql } from '@styles/theme'

export const CardWrapper = styled(Paper)({
  padding: '3rem',
  minHeight: '400px',
  maxHeight: '400px',
  margin: '7px auto',
  width: '100%',
  maxWidth: '500px',
  display: 'flex',
  userSelect: 'none',
  [mql['xs']]: {
    width: '90vw',
    maxWidth: '95vw',
  },
})

export const FormWrapper = styled('form')({
  display: 'flex',
  flexDirection: 'column',
})

export const FormFieldsWrapper = styled(Box)({
  display: 'flex',
  justifyContent: 'center',
  flexWrap: 'wrap',
  height: 'calc(100% - 3rem)',
  maxHeight: '400px',
  overflowY: 'scroll',
  '&::-webkit-scrollbar': {
    width: 8,
  },
  '&::-webkit-scrollbar-track': {
    backgroundColor: 'transparent',
  },
  '&::-webkit-scrollbar-thumb': {
    backgroundColor: '#DDE5F5',
    borderRadius: '6px',
  },
})

export const CardTitle = styled(Typography)({
  fontSize: '2rem',
  marginBottom: '6px',
})

export const CardSubTitle = styled(Typography)({
  marginBottom: '7px',
})

export const SubmitButton = styled(Button)({
  marginTop: 'auto',
  justifySelf: 'flex-end',
})

export const StopButton = styled(Button)({
  marginTop: '7px',
  justifySelf: 'flex-end',
})

SubmitButton.defaultProps = {
  variant: 'contained',
  type: 'submit',
}

StopButton.defaultProps = {
  variant: 'contained',
  color: 'error',
}

CardSubTitle.defaultProps = {
  variant: 'subtitle2',
}
