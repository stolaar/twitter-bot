import { useFormRenderer } from '@hooks/useFormRenderer'
import { useEffect, useMemo } from 'react'
import { TwitterFieldsFactory } from '../../../../formFields/TwitterFieldsFactory'
import {
  CardTitle,
  CardWrapper,
  FormFieldsWrapper,
  FormWrapper,
  SubmitButton,
} from '../styles'
import { getPosts } from '@store/forms/actions'
import { useDispatch } from 'react-redux'
import { FieldComponent } from '../../../common/FieldComponent/FieldComponent'
import { TITLES } from '@client-utils/constants/titles'
import { BUTTON_LABELS } from '@client-utils/constants/labels'

export const SchedulePostsForm = (): JSX.Element => {
  const dispatch = useDispatch()
  const {
    setFields,
    formState: { isDirty, isValid },
    handleSubmit,
    control,
    getValues,
  } = useFormRenderer()

  const twitterFields = useMemo(
    () => new TwitterFieldsFactory(control),
    [control],
  )

  useEffect(() => {
    if (twitterFields) {
      setFields([twitterFields.postText], [])
    }
  }, [twitterFields, setFields])

  const onSubmit = async () => {
    const { postText } = getValues()
    dispatch(getPosts(postText))
  }

  return (
    <CardWrapper>
      <FormWrapper onSubmit={handleSubmit(onSubmit)}>
        <CardTitle>{TITLES.schedulePosts}</CardTitle>
        <FormFieldsWrapper>
          <FieldComponent formField={twitterFields.postText} />
        </FormFieldsWrapper>
        <SubmitButton disabled={!isValid || !isDirty}>
          {BUTTON_LABELS.submit}
        </SubmitButton>
      </FormWrapper>
    </CardWrapper>
  )
}
