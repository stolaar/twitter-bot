import { FollowByHashtagsForm } from './FollowByHashtagsForm'
import { FollowByHashtagsTask } from './FollowByHashtagsTask'
import { useDispatch, useSelector } from 'react-redux'
import { IAppState } from '@client-interfaces/store'
import { useEffect } from 'react'
import { getTasks } from '@store/tasks/actions'
import { CardWrapper } from '../styles'

export const FollowByHashtags = (): JSX.Element => {
  const { followByHashtagTask, scheduleHashtagTask } = useSelector(
    (state: IAppState) => state.tasks,
  )
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getTasks())
  }, [dispatch])

  return (
    <CardWrapper>
      {scheduleHashtagTask || !followByHashtagTask ? (
        <FollowByHashtagsForm />
      ) : (
        <FollowByHashtagsTask task={followByHashtagTask} />
      )}
    </CardWrapper>
  )
}
