import { FC, useCallback, useEffect } from 'react'
import { Box, Typography } from '@mui/material'
import { ITask } from '@shared-interfaces/tasks.interfaces'
import { DateTimePicker } from '@components/common/DatePicker/DateTimePicker'
import { useFormRenderer } from '@hooks/useFormRenderer'
import { dateToISOFormat } from '@client-services/date'
import { FormFieldsWrapper, StopButton, SubmitButton } from '../styles'
import { BUTTON_LABELS } from '@client-utils/constants/labels'
import { TITLES } from '@client-utils/constants/titles'
import { getTasks, setScheduleHashtagForm } from '@store/tasks/actions'
import { useDispatch } from 'react-redux'

export const FollowByHashtagsTask: FC<{ task: ITask }> = ({
  task,
}): JSX.Element => {
  const { control, reset } = useFormRenderer()
  const dispatch = useDispatch()

  const onStopClicked = useCallback(async () => {
    await fetch('/twitter', { method: 'DELETE' })
    dispatch(getTasks())
  }, [])

  useEffect(() => {
    if (task) {
      reset({
        lastExecution: task.lastExecution
          ? dateToISOFormat(task.lastExecution)
          : null,
        nextExecution: task.nextExecution
          ? dateToISOFormat(task.nextExecution)
          : null,
      })
    }
  }, [task, reset])

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
      <Typography variant={'h6'} sx={{ mb: 4 }}>
        {TITLES.runningTaskInfo}
      </Typography>
      <FormFieldsWrapper>
        <DateTimePicker
          disabled
          control={control}
          label={'Last execution'}
          name={'lastExecution'}
          value={task.lastExecution ? new Date(task.lastExecution) : null}
          sx={{ width: '100%', flex: '0 1 100%' }}
        />
        <DateTimePicker
          disabled
          control={control}
          label={'Next execution'}
          name={'nextExecution'}
          value={task.nextExecution ? new Date(task.nextExecution) : null}
          sx={{ width: '100%', flex: '0 1 100%' }}
        />
      </FormFieldsWrapper>
      <SubmitButton onClick={() => dispatch(setScheduleHashtagForm(true))}>
        {BUTTON_LABELS.reSchedule}
      </SubmitButton>
      <StopButton onClick={onStopClicked}>{BUTTON_LABELS.stop}</StopButton>
    </Box>
  )
}
