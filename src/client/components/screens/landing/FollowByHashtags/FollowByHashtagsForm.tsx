import { useFormRenderer } from '@hooks/useFormRenderer'
import { useEffect, useMemo } from 'react'
import { TwitterFieldsFactory } from '../../../../formFields/TwitterFieldsFactory'
import { useDispatch } from 'react-redux'
import { getTasks } from '@store/tasks/actions'
import {
  CardSubTitle,
  CardTitle,
  FormFieldsWrapper,
  FormWrapper,
  SubmitButton,
} from '../styles'
import { FieldComponent } from '../../../common/FieldComponent/FieldComponent'
import { BUTTON_LABELS } from '@client-utils/constants/labels'
import { TITLES } from '@client-utils/constants/titles'

export const FollowByHashtagsForm = (): JSX.Element => {
  const dispatch = useDispatch()

  const {
    setFields,
    formState: { isValid, isDirty },
    handleSubmit,
    control,
    getValues,
  } = useFormRenderer()

  const twitterFields = useMemo(
    () => new TwitterFieldsFactory(control),
    [control],
  )

  useEffect(() => {
    if (twitterFields) {
      setFields([twitterFields.hashtags, twitterFields.cronExpression], [])
    }
  }, [twitterFields, setFields])

  const onSubmit = async () => {
    await fetch('/twitter/follow-tags', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        tags: getValues().hashtags?.split(','),
        cronExpression: getValues().cronExpression,
      }),
    })
    dispatch(getTasks())
  }

  return (
    <>
      <FormWrapper onSubmit={handleSubmit(onSubmit)}>
        <CardTitle>{TITLES.followByHashTag}</CardTitle>
        <CardSubTitle>{TITLES.followByHashTagSubtitle}</CardSubTitle>
        <FormFieldsWrapper>
          <FieldComponent formField={twitterFields.hashtags} />
          <FieldComponent formField={twitterFields.cronExpression} />
        </FormFieldsWrapper>
        <SubmitButton disabled={!isValid || !isDirty}>
          {BUTTON_LABELS.submit}
        </SubmitButton>
      </FormWrapper>
    </>
  )
}
