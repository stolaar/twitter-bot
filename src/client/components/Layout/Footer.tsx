import { Box } from '@mui/material'

export const Footer = (): JSX.Element => {
  return (
    <Box
      sx={({ palette }) => ({
        background: palette.background.paper,
        height: '40px',
        padding: '0.5rem 0',
      })}
    >
      Powered by Stolar
    </Box>
  )
}
