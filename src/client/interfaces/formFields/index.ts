import { FormField } from '../../formFields/common/FormField'

export interface IFieldFactory<FieldKey extends string> {
  fields: Record<FieldKey, FormField>
}

export type DynamicFactory<K extends string> = Record<K, FormField>

export interface DynamicFactoryConstructor<T extends string> {
  new (
    t: (key: string) => string,
    getParameterValue: (code: string) => any,
  ): DynamicFactory<T>
}

export interface IFieldFactoryConstructor<T extends string> {
  new (
    t: (key: string) => string,
    getParameterValue: (code: string) => any,
  ): IFieldFactory<T>
}

export type TwitterFieldKeys =
  | 'hashtags'
  | 'query'
  | 'cronExpression'
  | 'postMedia'
  | 'postText'
