import { ITask } from '@shared-interfaces/tasks.interfaces'

export interface IAuthState {
  isAuthenticated: boolean
}

export interface IOption {
  label: string
  value: string | number
}

export interface IFormState {
  locked: boolean
  optionsOne: IOption[]
  optionsTwo: IOption[]
}

export interface IAppState {
  auth: IAuthState
  form: IFormState
  tasks: ITasksState
}

export interface ITasksState {
  tasks: ITask[]
  followByHashtagTask?: ITask
  scheduleHashtagTask?: boolean
}
