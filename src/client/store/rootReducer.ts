import { combineReducers } from 'redux'
import { reducer as authReducer } from './auth/reducer'
import { reducer as formReducer } from './forms/reducer'
import { reducer as tasksReducer } from './tasks/reducer'
import { IAppState } from '../interfaces/store'

export const rootReducer = combineReducers<IAppState>({
  auth: authReducer,
  form: formReducer,
  tasks: tasksReducer,
})
