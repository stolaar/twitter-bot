import { Reducer } from 'redux'
import { AuthActions } from './actions'
import { SET_IS_AUTHENTICATED } from './actionTypes'
import { IAuthState } from '../../interfaces/store'

const initialState: IAuthState = {
  isAuthenticated: false,
}

export const reducer: Reducer<IAuthState, AuthActions> = (
  state: IAuthState = initialState,
  action,
) => {
  switch (action.type) {
    case SET_IS_AUTHENTICATED:
      return { ...state, isAuthenticated: action.payload }
    default:
      return state
  }
}
