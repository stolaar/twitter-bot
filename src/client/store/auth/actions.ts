import { Action } from 'redux'
import { SET_IS_AUTHENTICATED } from './actionTypes'

export interface ISetAuthenticatedAction
  extends Action<typeof SET_IS_AUTHENTICATED> {
  payload: boolean
}

export const setIsAuthenticated = (payload: boolean) => ({
  type: SET_IS_AUTHENTICATED,
  payload,
})

export type AuthActions = ISetAuthenticatedAction
