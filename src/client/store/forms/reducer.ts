import { Reducer } from 'redux'
import { FormActions } from './actions'
import { SET_OPTIONS_ONE, SET_OPTIONS_TWO } from './actionTypes'
import { IFormState } from '../../interfaces/store'

const initialState: IFormState = {
  locked: false,
  optionsOne: [],
  optionsTwo: [],
}

export const reducer: Reducer<IFormState, FormActions> = (
  state: IFormState = initialState,
  action,
) => {
  switch (action.type) {
    case SET_OPTIONS_ONE:
      return { ...state, optionsOne: action.payload }
    case SET_OPTIONS_TWO:
      return { ...state, optionsTwo: action.payload }
    default:
      return state
  }
}
