import { Action, Dispatch } from 'redux'
import { SET_OPTIONS_ONE, SET_OPTIONS_TWO } from './actionTypes'
import { IOption } from '../../interfaces/store'
import axios from 'axios'

export interface ISetOptionsOne extends Action<typeof SET_OPTIONS_ONE> {
  payload: IOption[]
}
export interface ISetOptionsTwo extends Action<typeof SET_OPTIONS_TWO> {
  payload: IOption[]
}

export const setOptionsOne = (payload: IOption[]) => ({
  type: SET_OPTIONS_ONE,
  payload,
})

export const setOptionsTwo = (payload: IOption[]) => ({
  type: SET_OPTIONS_TWO,
  payload,
})

export const getOptionsOne = () => (dispatch: Dispatch) => {
  setTimeout(() => {
    dispatch(
      setOptionsOne([
        {
          label: 'Labela 1',
          value: 1,
        },
        {
          label: 'labela 2',
          value: 2,
        },
      ]),
    )
  }, 1500)
}

export const getPosts = (text: string) => async (_: Dispatch) => {
  try {
    const { data } = await axios.post('/posts', { text })
    console.log('data', data)
  } catch (err) {
    console.error(err)
  }
}

export type FormActions = ISetOptionsOne | ISetOptionsTwo
