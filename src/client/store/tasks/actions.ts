import { Action, Dispatch } from 'redux'
import {
  SET_FOLLOW_BY_HASHTAG_TASK,
  SET_SCHEDULE_HASHTAG_FORM,
  SET_TASKS,
} from './actionTypes'
import { ITask } from '@shared-interfaces/tasks.interfaces'
import twitterConstants from '@shared-constants/twitter.constants'

export interface ISetTasks extends Action<typeof SET_TASKS> {
  payload: ITask[]
}

export interface ISetFollowByHashtagTask
  extends Action<typeof SET_FOLLOW_BY_HASHTAG_TASK> {
  payload: ITask
}
export interface ISetScheduleHashtagForm
  extends Action<typeof SET_SCHEDULE_HASHTAG_FORM> {
  payload: boolean
}

export const getTasks = () => async (dispatch: Dispatch) => {
  try {
    const response = await fetch('/twitter')
    const tasks = await response.json()
    const followByHashtagsTask = tasks.find(
      (task) => task.name === twitterConstants.cronTaskNames().followTags,
    )
    dispatch(setTasks(tasks))
    dispatch(setFollowByHashtagTask(followByHashtagsTask))
  } catch (err) {
    console.error(err)
  }
}

export const setTasks = (tasks: ITask[]) => ({
  type: SET_TASKS,
  payload: tasks,
})

export const setFollowByHashtagTask = (task: ITask) => ({
  type: SET_FOLLOW_BY_HASHTAG_TASK,
  payload: task,
})

export const setScheduleHashtagForm = (payload: boolean) => ({
  type: SET_SCHEDULE_HASHTAG_FORM,
  payload,
})

export type FormActions =
  | ISetTasks
  | ISetFollowByHashtagTask
  | ISetScheduleHashtagForm
