import { Reducer } from 'redux'
import { FormActions } from './actions'
import {
  SET_FOLLOW_BY_HASHTAG_TASK,
  SET_SCHEDULE_HASHTAG_FORM,
  SET_TASKS,
} from './actionTypes'
import { ITasksState } from '../../interfaces/store'

const initialState: ITasksState = {
  tasks: [],
  followByHashtagTask: null,
  scheduleHashtagTask: true,
}

export const reducer: Reducer<ITasksState, FormActions> = (
  state: ITasksState = initialState,
  action,
) => {
  switch (action.type) {
    case SET_TASKS:
      return { ...state, tasks: action.payload }
    case SET_FOLLOW_BY_HASHTAG_TASK:
      return {
        ...state,
        followByHashtagTask: action.payload,
        scheduleHashtagTask: false,
      }
    case SET_SCHEDULE_HASHTAG_FORM:
      return { ...state, scheduleHashtagTask: action.payload }
    default:
      return state
  }
}
