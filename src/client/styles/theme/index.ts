import { createTheme } from '@mui/material'

const BREAKPOINTS = {
  xs: 420,
  sm: 576,
  md: 768,
  lg: 900,
  xl: 1200,
  xxl: 1536,
}

type Mq = keyof typeof BREAKPOINTS

export const mql = Object.keys(BREAKPOINTS)
  .map((key) => [key, BREAKPOINTS[key as Mq]] as [Mq, number])
  .reduce((prev, [key, breakpoint]) => {
    prev[key as string] = `@media (max-width: ${breakpoint}px)`
    return prev
  }, {} as Record<Mq, string>)

export default createTheme({
  palette: {
    mode: 'dark',
  },
})
