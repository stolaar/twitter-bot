import { Control } from 'react-hook-form'

export class BaseFieldsFactory<T extends string> {
  constructor(private control: Control) {}

  commonProps(name: T) {
    return {
      name,
      sx: { margin: '5px auto 5px auto', width: '100%' },
    }
  }

  addCommon(props: any) {
    Object.keys(this).forEach((key) => {
      this[key as string].addProps && this[key as string].addProps(props)
    })
  }
}
