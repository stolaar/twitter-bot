import { FormField } from './common/FormField'
import { DynamicFactory, TwitterFieldKeys } from '../interfaces/formFields'
import { Input, InputProps, TextFieldProps } from '@mui/material'
import * as Yup from 'yup'
import {
  ITextFieldProps,
  TextField,
} from '@components/common/TextField/TextField'
import { BaseFieldsFactory } from './BaseFieldsFactory'
import { Control } from 'react-hook-form'
import {
  FIELD_HELPER_TEXTS,
  FIELD_LABELS,
  FIELD_PLACEHOLDERS,
} from '../utils/constants/labels'

const cronRegex =
  // eslint-disable-next-line security/detect-unsafe-regex
  /^((((\d+,)+\d+|(\d+(\/|-|#)\d+)|\d+L?|\*(\/\d+)?|L(-\d+)?|\?|[A-Z]{3}(-[A-Z]{3})?) ?){5,7})$|(@(annually|yearly|monthly|weekly|daily|hourly|reboot))|(@every (\d+(ns|us|µs|ms|s|m|h))+)/

export class TwitterFieldsFactory
  extends BaseFieldsFactory<TwitterFieldKeys>
  implements DynamicFactory<TwitterFieldKeys>
{
  hashtags: FormField<TextFieldProps>
  query: FormField<TextFieldProps>
  cronExpression: FormField<TextFieldProps>
  postText: FormField<TextFieldProps>
  postMedia: FormField<InputProps>

  cyclicValidations: [string, string][]
  constructor(control: Control) {
    super(control)
    this.cyclicValidations = []
    this.hashtags = new FormField<ITextFieldProps>(
      TextField,
      Yup.string().required().min(4).matches(/^#/, 'Has to start with #'),
      {
        ...this.commonProps('hashtags'),
        control,
        required: true,
        label: FIELD_LABELS.hashtags,
        helperText: FIELD_HELPER_TEXTS.hashtags,
        placeholder: FIELD_PLACEHOLDERS.hashtags,
      },
    )

    this.query = new FormField<ITextFieldProps>(
      TextField,
      Yup.string().min(2),
      {
        ...this.commonProps('query'),
        control,
        label: FIELD_LABELS.query,
      },
    )

    this.cronExpression = new FormField<ITextFieldProps>(
      TextField,
      Yup.string().required().matches(cronRegex, 'Invalid cron expression'),
      {
        ...this.commonProps('cronExpression'),
        control,
        required: true,
        label: FIELD_LABELS.cron,
        placeholder: FIELD_PLACEHOLDERS.cron,
        helperText: FIELD_HELPER_TEXTS.cron,
      },
    )

    this.postText = new FormField<ITextFieldProps>(
      TextField,
      Yup.string().required(),
      {
        ...this.commonProps('postText'),
        control,
        label: FIELD_LABELS.postText,
      },
    )

    this.postMedia = new FormField<any>(Input, Yup.string().required(), {
      ...this.commonProps('postMedia'),
      control,
      type: 'file',
      label: FIELD_LABELS.postMedia,
    })
  }
}
