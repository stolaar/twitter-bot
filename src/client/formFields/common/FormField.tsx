import { SxProps } from '@mui/material'
import { FC } from 'react'
import {
  DateSchema,
  NumberSchema,
  StringSchema,
  AnyObjectSchema,
  AnySchema,
} from 'yup'

export type FieldValidationType =
  | StringSchema<string | undefined, AnyObjectSchema, string | undefined>
  | NumberSchema<number | undefined, AnyObjectSchema, number | undefined>
  | DateSchema<Date | undefined, AnyObjectSchema, Date | undefined>
  | false
  | any

export class FormField<
  Props extends { name?: string; sx?: SxProps } = Record<string, any>,
> {
  component: FC<Props>

  validation: FieldValidationType

  props: Props = {} as Record<string, any> as Props

  hidden: boolean

  render<T = Props>(props?: Partial<T>) {
    if (this.hidden) return null
    const combinedProps = { ...this.props, ...props }
    const Component = this.component
    return <Component {...combinedProps} />
  }

  constructor(
    component: FC<Props>,
    validation: FieldValidationType,
    defaultProps?: Props,
    hidden?: boolean,
  ) {
    this.component = component
    this.validation = validation
    this.hidden = hidden || false
    if (defaultProps) this.props = defaultProps
  }

  addProps(props: Props): this {
    this.props = { ...this.props, ...props }
    return this
  }

  appendSx(sxProps: SxProps): this {
    this.props = { ...this.props, sx: { ...this.props?.sx, ...sxProps } }
    return this
  }

  getName(): string {
    return (this.props?.name as string) || ''
  }

  getLabel(): string {
    return ((this.props as any)?.label as string) || ''
  }

  getProps(): Props {
    return this.props
  }

  useComponent(component: FC<any>): this {
    this.component = component
    return this
  }

  addValidation({
    validation,
    override = false,
  }: {
    validation: AnySchema
    override?: boolean
  }): this {
    if (override) {
      this.validation = validation
    } else {
      this.validation = this.validation.concat(validation)
    }

    return this
  }

  hide(shouldHide: boolean): this {
    this.hidden = shouldHide
    return this
  }
}
