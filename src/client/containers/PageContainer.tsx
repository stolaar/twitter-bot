import { ComponentProps } from '../interfaces/ComponentProps'

export const PageContainer: React.FC<ComponentProps> = ({
  children,
}): JSX.Element => {
  return <>{children}</>
}
