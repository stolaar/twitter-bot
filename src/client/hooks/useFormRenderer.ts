import { useCallback, useEffect, useState } from 'react'
import { FormField } from '../formFields/common/FormField'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'

export const useFormRenderer = () => {
  const [schema, setSchema] = useState<any>(Yup.object())
  const resolver = yupResolver(schema)
  const [fieldsUsed, setFieldsUsed] = useState<FormField[]>([])
  const [dependentKeys, setDependentKeys] = useState<[string, string][]>([])

  type FormData = Yup.InferType<typeof schema>

  const [elements, setElements] = useState<(JSX.Element | null)[]>([])

  const { register, handleSubmit, formState, reset, control, ...restMethods } =
    useForm<FormData>({
      resolver,
      mode: 'onBlur',
      criteriaMode: 'all',
    })

  const setFields = useCallback(
    (fields: FormField[], dependenKey: [string, string][]) => {
      if (fields && fields.length) {
        setFieldsUsed(fields)
      }

      if (dependenKey.length) {
        setDependentKeys(dependenKey)
      }
    },
    [],
  )

  useEffect(() => {
    if (fieldsUsed.length) {
      setElements(
        fieldsUsed.map((field, index) => {
          return field.render({ key: index })
        }),
      )
      const schema = fieldsUsed
        .filter((field) => !field.hidden)
        .reduce((acc: Record<string, any>, curr) => {
          acc[curr.getName()] = curr.validation
          return acc
        }, {})
      setSchema(Yup.object().shape(schema, dependentKeys))
    }
  }, [fieldsUsed, dependentKeys])

  return {
    elements,
    setFields,
    register,
    formState,
    handleSubmit,
    reset,
    control,
    setFieldsUsed,
    ...restMethods,
  }
}
