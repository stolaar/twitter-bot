import { Module } from '@nestjs/common'
import { TwitterBotService } from './twitter-bot.service'
import { ConfigModule } from '@nestjs/config'
import twitterConfig from '../config/twitter.config'

@Module({
  imports: [ConfigModule.forRoot({ load: [twitterConfig] })],
  providers: [TwitterBotService],
  exports: [TwitterBotService],
})
export class TwitterBotModule {}
