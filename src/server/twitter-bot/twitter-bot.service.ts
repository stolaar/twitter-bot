import { Inject, Injectable } from '@nestjs/common'
import Twit from 'twit'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class TwitterBotService {
  private readonly twitterClient: Twit

  constructor(@Inject(ConfigService) private readonly config: ConfigService) {
    this.twitterClient = Twit(this.config.get('twitter'))
  }
  async followByHashTags(tags: string[]): Promise<string> {
    console.log('Try to search by tags')
    const { data } = await this.twitterClient.get('search/tweets', {
      q: tags.join(','),
      count: 5,
      result_type: 'recent',
    })
    console.log('Taks data', data.statuses)
    await Promise.all(
      data.statuses.map(async (tweet) => {
        const { id_str } = tweet.user
        await this.followUser(id_str)
      }),
    )
    return tags.join(',')
  }

  async followUser(userId: string): Promise<void> {
    try {
      await this.twitterClient.post('friendships/create', {
        user_id: userId,
        follow: true,
      })
    } catch (error) {
      console.log('Error followUser', error)
    }
  }
}
