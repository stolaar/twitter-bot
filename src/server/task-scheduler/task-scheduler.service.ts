import { Injectable } from '@nestjs/common'
import { SchedulerRegistry } from '@nestjs/schedule'
import { CronJob, CronJobParameters, CronTime } from 'cron'
import { ITask } from '@shared-interfaces/tasks.interfaces'

@Injectable()
export class TaskSchedulerService extends SchedulerRegistry {
  constructor() {
    super()
  }

  startTask(taskName: string, cronJobsParameters: CronJobParameters) {
    this.addCronJob(taskName, new CronJob(cronJobsParameters))
  }

  getTasksByPrefix(taskName: string) {
    return Array.from(this.getCronJobs().entries())
      .filter(([key]) => key.startsWith(taskName))
      .map(
        ([key, value]) =>
          ({
            name: key,
            nextExecution: value.nextDates().toString(),
            lastExecution: value.lastDate()?.toString(),
          } as ITask),
      )
  }

  scheduleOrRescheduleTask(
    taskName: string,
    cronJobsParameters: CronJobParameters,
  ) {
    try {
      const task = this.getCronJob(taskName)
      if (task) {
        console.log('Set time', cronJobsParameters.cronTime)
        task.setTime(new CronTime(cronJobsParameters.cronTime))
      }
    } catch (_) {
      /** Do nothing */
    }
    console.log('Start task', cronJobsParameters)
    this.startTask(taskName, cronJobsParameters)
  }
}
