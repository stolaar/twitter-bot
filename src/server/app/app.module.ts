import { Module } from '@nestjs/common'
import { AppService } from './app.service'
import { TwitterTasksModule } from '../twitter-tasks/twitter-tasks.module'
import { AppController } from './app.controller'
import { RenderModule } from 'nest-next'
import Next from 'next'
import { PostsGeneratorModule } from '../posts-generator/posts-generator.module'

@Module({
  imports: [
    PostsGeneratorModule,
    TwitterTasksModule,
    RenderModule.forRootAsync(Next({ dev: true }), {
      viewsDir: null,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
