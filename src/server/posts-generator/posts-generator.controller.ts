import { Body, Controller, Post } from '@nestjs/common'
import { PostsGeneratorService } from './posts-generator.service'

@Controller('posts')
export class PostsGeneratorController {
  constructor(private readonly postsGeneratorService: PostsGeneratorService) {}

  @Post()
  async generatePosts(@Body('text') text: string) {
    return this.postsGeneratorService.generatePosts(text)
  }
}
