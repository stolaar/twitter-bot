import { Module } from '@nestjs/common'
import { PostsGeneratorService } from './posts-generator.service'
import { PostsGeneratorController } from './posts-generator.controller'

@Module({
  controllers: [PostsGeneratorController],
  providers: [PostsGeneratorService],
  exports: [PostsGeneratorService],
})
export class PostsGeneratorModule {}
