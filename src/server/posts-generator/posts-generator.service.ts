import { Injectable } from '@nestjs/common'
import { Wit } from 'node-wit'

@Injectable()
export class PostsGeneratorService {
  private readonly witClient: Wit

  constructor() {
    this.witClient = new Wit({
      accessToken: process.env.WIT_TOKEN,
    })
  }

  async generatePosts(text: string): Promise<string[]> {
    const response = await this.witClient.message(text)
    const entities: Record<string, any> = response.entities
    const posts = []

    Object.entries(entities).forEach(([entity, values]) => {
      if (entity === 'intent') {
        values.forEach((value) => {
          if (value.value.includes('share')) {
            posts.push(text)
          }
        })
      }
    })

    return posts
  }
}
