import { Module } from '@nestjs/common'
import { TwitterBotModule } from '../twitter-bot/twitter-bot.module'
import { TwitterTasksController } from './twitter-tasks.controller'
import { TwitterTasksService } from './twitter-tasks.service'
import { TaskSchedulerModule } from '../task-scheduler/task-scheduler.module'

@Module({
  imports: [TwitterBotModule, TaskSchedulerModule],
  controllers: [TwitterTasksController],
  providers: [TwitterTasksService],
})
export class TwitterTasksModule {}
