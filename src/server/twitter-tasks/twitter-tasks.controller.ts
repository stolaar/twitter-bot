import { Controller, Post, Body, Get, Inject, Delete } from '@nestjs/common'
import { TwitterTasksService } from './twitter-tasks.service'

@Controller('/twitter')
export class TwitterTasksController {
  @Inject(TwitterTasksService)
  private readonly taskScheduler: TwitterTasksService

  @Post('/follow-tags')
  async followTags(
    @Body('cronExpression') cronExpression: string,
    @Body('tags') tags: string[],
  ) {
    return this.taskScheduler.followByHashTags(cronExpression, tags)
  }

  @Get('/')
  async getTwitterTasks() {
    return this.taskScheduler.getTwitterTasks()
  }

  @Delete('/')
  async deleteFollowByHashTagsTask() {
    return this.taskScheduler.deleteFollowByHashTagsTask()
  }
}
