import { HttpException, Inject, Injectable } from '@nestjs/common'
import { TwitterBotService } from '../twitter-bot/twitter-bot.service'
import twitterConstants from '../../shared/constants/twitter.constants'
import { ITask } from '@shared-interfaces/tasks.interfaces'
import { TaskSchedulerService } from '../task-scheduler/task-scheduler.service'

@Injectable()
export class TwitterTasksService {
  private readonly cronNames = twitterConstants.cronTaskNames()

  constructor(
    @Inject(TaskSchedulerService)
    private readonly taskSchedulerService: TaskSchedulerService,
    @Inject(TwitterBotService)
    private readonly twitterBotService: TwitterBotService,
  ) {}

  getTwitterTasks(): ITask[] {
    return this.taskSchedulerService.getTasksByPrefix(
      twitterConstants.TASK_PREFIX,
    )
  }

  async followByHashTags(cronExpression: string, tags: string[]) {
    try {
      this.taskSchedulerService.scheduleOrRescheduleTask(
        this.cronNames.followTags,
        {
          cronTime: cronExpression,
          start: true,
          onTick: async () => {
            await this.twitterBotService.followByHashTags(tags)
          },
        },
      )
    } catch (error) {
      throw new HttpException({ message: error }, 400)
    }
  }

  deleteFollowByHashTagsTask() {
    this.taskSchedulerService.deleteCronJob(this.cronNames.followTags)
  }
}
