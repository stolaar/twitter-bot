import React, { FC } from 'react'
import styles from '../client/styles/home.module.css'
import { FollowByHashtags } from '@components/screens/landing/FollowByHashtags/FollowByHashtags'
import { Box } from '@mui/material'
import { SchedulePostsForm } from '@components/screens/landing/SchedulePosts/SchedulePostsForm'
import { RetweetPostsForm } from '@components/screens/landing/RetweetPosts/RetweetPostsForm'

const Home: FC = () => {
  return (
    <Box
      className={styles.main}
      sx={({ palette }) => ({
        backgroundColor: palette.background.paper,
        padding: 10,
      })}
    >
      <FollowByHashtags />
      <SchedulePostsForm />
      <RetweetPostsForm />
    </Box>
  )
}

export default Home
