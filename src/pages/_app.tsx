import React, { FC } from 'react'
import { AppProps } from 'next/app'
import { AppLayout } from '@components/Layout/AppLayout'
import '../client/styles/globals.css'
import { store } from '@store/index'
import { Provider as ReduxProvider } from 'react-redux'
import { ThemeProvider } from '@mui/material'
import theme from '../client/styles/theme'

const app: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ReduxProvider store={store}>
      <ThemeProvider theme={theme}>
        <AppLayout>
          <Component {...pageProps} />
        </AppLayout>
      </ThemeProvider>
    </ReduxProvider>
  )
}

export default app
