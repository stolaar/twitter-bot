export interface ITask {
  name: string
  nextExecution: string
  lastExecution: string
}
