export default {
  TASK_PREFIX: 'twitter',
  cronTaskNames: function () {
    return { followTags: `${this.TASK_PREFIX}-follow-tags` }
  },
}
